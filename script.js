// Прототипное наследование позволяет расширить объекты методами и свойствами,
// другими словами - при наследовании пртотипа другим объектом ему передадуться и свойства
// и методы прототипа, что позволяет применять это в повторяющихся объектах.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set name(name) {
    return (this._name = name);
  }

  get name() {
    return this._name;
  }
  set age(age) {
    return (this._age = age);
  }

  get age() {
    return this._age;
  }
  set salary(salary) {
    return (this._salary = salary);
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(salary) {
    this._salary = salary * 3;
  }

  get salary() {
    return this._salary;
  }
}

let anton = new Programmer("anton", 35, 200, "Ukrainian, English");
console.log(anton);
let tanya = new Programmer("tanya", 36, 500, "Japanise, English, Deutsch");
console.log(tanya);
